# aCMG

antiX community MOC GUI is a script providing easy desktop access to MOC (Music On Console) Player/Server.

## Installation
There are .deb installer packages available. Make sure to check the sha256 sums after download before installing.

## Status:
Under Construction

## License:
GPL v.3

## Contributions:
Translations, Suggestions, Questions, Bug reporting please to https://www.antixforum.com

___
Written 2023 by Robin for antiX Community
